import com.badlogic.gdx.Input.Keys
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType
import com.badlogic.gdx.graphics.{Color, GL20}
import com.badlogic.gdx.{Game, Gdx}
import domain.entities
import domain.entities.{Down, Food, Frame, Left, Right, Up}
import domain.entities.typeClasses.Handle._
import domain.entities.typeClasses.ToPoints._
import domain.entities.typeClasses.Update._

class SnakeGame(var game: entities.Game, val sizeMultiplayer: Float, val random: scala.util.Random) extends Game {
  lazy val prs = new InputCondensate
  lazy val shapeRenderer: ShapeRenderer = new ShapeRenderer()
  implicit val rnd: scala.util.Random = random

  override def create(): Unit = {
    Gdx.input.setInputProcessor(prs)
  }

  override def render(): Unit = {
    val input = prs.list.map(i => i match {
      case Keys.UP => Up
      case Keys.DOWN => Down
      case Keys.LEFT => Left
      case Keys.RIGHT => Right
    })
    game = game
      .handle(input)
      .update(Gdx.graphics.getDeltaTime(), rnd)
      .unsafeRunSync()
    prs.clear()
    Gdx.gl.glClearColor(1, 1, 1, 1)
    Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)
    shapeRenderer.setColor(Color.BLACK)
    shapeRenderer.begin(ShapeType.Filled)
    for (p <- game.toPoints().toList)
      shapeRenderer.circle(p.x * sizeMultiplayer, p.y * sizeMultiplayer, sizeMultiplayer / 2)
    shapeRenderer.end()
  }
}


