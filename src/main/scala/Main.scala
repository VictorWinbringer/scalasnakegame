import com.badlogic.gdx.backends.lwjgl.{LwjglApplication, LwjglApplicationConfiguration}
import domain.entities
import domain.entities.{Food, Frame, Game, Point, Right, Snake}

import scala.util.Random

object Main extends App {
  val config = new LwjglApplicationConfiguration
  config.title = "Scala Snake Game"
  config.width = 300
  config.height = 300
  val food = Food(Point(4, 4))
  val frame = Frame(entities.Point(0, 0), entities.Point(30, 30))
  val snake = Snake(entities.Point(5, 5) :: entities.Point(6, 6) :: entities.Point(7, 7) :: Nil, Right)
  val game = Game(food, snake, frame, 0, snake)
  new LwjglApplication(new SnakeGame(game, 10, new Random()), config)
}
