package domain.entities

case class Point(x: Int, y: Int)
