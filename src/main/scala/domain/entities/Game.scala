package domain.entities

case class Game(food: Food, snake: Snake, frame: Frame, elapsedTime: Float, start: Snake)
