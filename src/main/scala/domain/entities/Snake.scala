package domain.entities

case class Snake(body: List[Point], direction: Direction)
