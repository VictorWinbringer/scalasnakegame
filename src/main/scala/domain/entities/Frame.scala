package domain.entities

case class Frame(min: Point, max: Point)
