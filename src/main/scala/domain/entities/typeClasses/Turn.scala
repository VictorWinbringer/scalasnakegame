package domain.entities.typeClasses

import domain.entities.{Direction, Snake}

trait Turn[A] {
  def turn(direction: Direction): A
}

object Turn {

  implicit class SnakeTurn(snake: Snake) extends Turn[Snake] {
    override def turn(direction: Direction): Snake = {
      snake.copy(direction = direction)
    }
  }

}