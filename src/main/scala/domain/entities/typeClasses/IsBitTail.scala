package domain.entities.typeClasses

import domain.entities.Snake

trait IsBitTail[A] {
  def isBitTail(): Boolean
}

object IsBitTail {

  implicit class IsBitTailService(snake: Snake) extends IsBitTail[Snake] {
    def isBitTail() = {
      snake.body.tail.exists(p => p == snake.body.head)
    }
  }

}