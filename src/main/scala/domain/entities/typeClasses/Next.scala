package domain.entities.typeClasses

import cats.effect.IO
import domain.entities.{Frame, Point}

import scala.util.Random

trait Next[F[_], A, B, C] {
  def next(b: B): F[C]
}

object Next {

  implicit class NextExtensions(random: Random) extends Next[IO, Random, Frame, Point] {
    def next(frame: Frame): IO[Point] = IO {
      val x = random.between(frame.min.x + 1, frame.max.x)
      val y = random.between(frame.min.y + 1, frame.max.y)
      Point(x, y)
    }
  }

}