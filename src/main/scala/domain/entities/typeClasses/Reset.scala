package domain.entities.typeClasses

import domain.entities.Game

trait Reset[A] {
  def reset(): A
}

object Reset {

  implicit class ResetServices(game: Game) extends Reset[Game]{
    def reset() = game.copy(snake = game.start)
  }
}