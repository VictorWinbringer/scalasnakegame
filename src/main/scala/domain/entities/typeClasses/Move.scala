package domain.entities.typeClasses

import domain.entities.{Down, Left, Right, Snake, Up}

trait Move[A] {
  def move(): A
}

object Move {

  implicit class MoveService(snake: Snake) extends Move[Snake] {
    def move(): Snake = {
      val point = snake.direction match {
        case Up => snake.body.head.copy(y = snake.body.head.y + 1)
        case Down => snake.body.head.copy(y = snake.body.head.y - 1)
        case Left => snake.body.head.copy(x = snake.body.head.x - 1)
        case Right => snake.body.head.copy(x = snake.body.head.x + 1)
      }
      snake.copy(body = point :: snake.body.filter(p => !p.eq(snake.body.last)))
    }
  }

}