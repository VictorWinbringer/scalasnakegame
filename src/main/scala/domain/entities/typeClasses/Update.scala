package domain.entities.typeClasses

import cats.effect.IO
import domain.entities.Game
import domain.entities.typeClasses.Move._
import domain.entities.typeClasses.HeadIsIn._
import domain.entities.typeClasses.Reset._
import domain.entities.typeClasses.IsBitTail._
import domain.entities.typeClasses.CanEat._
import domain.entities.typeClasses.Eat._
import domain.entities.typeClasses.MoveTo._
import domain.entities.typeClasses.Next._
import scala.util.Random

trait Update[F[_], A, B] {
  def update(deltaTime: Float, b: B): F[A]
}

object Update {

  implicit class UpdateServices(oldGame: Game) extends Update[IO, Game, Random] {
    override def update(deltaTime: Float, random: Random): IO[Game] = {
      val elapsed = oldGame.elapsedTime + deltaTime
      if (elapsed > 0.1) {
        val game = oldGame.copy(snake = oldGame.snake.move(), elapsedTime = 0)
        if (!game.snake.headIsIn(game.frame)) {
          IO(game.reset())
        } else if (game.snake.isBitTail()) {
          IO(game.reset())
        } else if (game.snake.canEat(game.food)) {
          random.next(game.frame).map(x => game.copy(snake = game.snake.eat(game.food), food = game.food.moveTo(x)))
        } else {
          IO(game)
        }
      } else {
        IO(oldGame.copy(elapsedTime = elapsed))
      }
    }
  }

}