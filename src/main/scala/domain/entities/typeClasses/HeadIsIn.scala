package domain.entities.typeClasses

import domain.entities.{Frame, Snake}

trait HeadIsIn[A] {
  def headIsIn(frame: Frame): Boolean
}

object HeadIsIn {

  implicit class HeadIsInService(snake: Snake) extends HeadIsIn[Snake] {
    def headIsIn(frame: Frame): Boolean = {
      snake.body.head.x < frame.max.x &&
        snake.body.head.y < frame.max.y &&
        snake.body.head.x > frame.min.x &&
        snake.body.head.y > frame.min.y
    }
  }

}