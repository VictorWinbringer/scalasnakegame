package domain.entities.typeClasses

import domain.entities.{Food, Snake}

trait CanEat[A] {
  def canEat(food: Food): Boolean
}

object CanEat {

  implicit class CanEatService(snake: Snake) extends CanEat[Snake] {
    def canEat(food: Food):Boolean = {
      snake.body.head == food.body
    }
  }
}