package domain.entities.typeClasses

import domain.entities.{Food, Snake}

trait Eat[A] {
  def eat(food: Food): A
}

object Eat {

  implicit class eatService(snake: Snake) extends Eat[Snake] {
    def eat(food: Food): Snake = {
      snake.copy(body = food.body :: snake.body)
    }
  }
}