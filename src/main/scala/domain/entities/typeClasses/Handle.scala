package domain.entities.typeClasses

import domain.entities.{Direction, Game}
import domain.entities.typeClasses.Turn._

trait Handle[A] {
  def handle(input: List[Direction]): A
}

object Handle {

  implicit class HandleServices(game: Game) extends Handle[Game] {
    def handle(input: List[Direction]): Game = {
      if (input.isEmpty) {
        game
      } else {
        game.copy(snake = input.foldLeft(game.snake)((s, d) => s.turn(d)))
      }
    }
  }
}