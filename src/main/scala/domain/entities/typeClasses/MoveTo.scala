package domain.entities.typeClasses

import domain.entities.{Food, Point}

trait MoveTo[A, B] {
  def moveTo(b: B): A
}

object MoveTo {

  implicit class MoveToServices(a: Food) extends MoveTo[Food, Point]{
    def moveTo(b: Point): Food = a.copy(body = b)
  }
}