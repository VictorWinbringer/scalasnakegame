package domain.entities.typeClasses

import domain.entities.{Frame, Game, Point}

trait ToPoints[A] {
  def toPoints(): Seq[Point]
}

object ToPoints {
  implicit class FrameToPoints(frame: Frame) extends ToPoints[Frame] {
    def toPoints(): Seq[Point] = {
      for (i <- frame.min.x until frame.max.x + 1;
           j <- frame.min.y until frame.max.y + 1
           if i == frame.min.x ||
             i == frame.max.x ||
             j == frame.min.y ||
             j == frame.max.y)
        yield Point(i, j)
    }
  }

  implicit class GameToPoints(game: Game) extends ToPoints[Game] {
    def toPoints(): Seq[Point] = {
      (game.food.body :: game.snake.body) ::: game.frame.toPoints().toList
    }
  }

}